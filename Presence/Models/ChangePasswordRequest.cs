﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Models
{
    public class ChangePasswordRequest
    {
        public string UserID { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }
}