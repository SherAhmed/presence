﻿using Presence.DataAccess.Models;
using Presence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Services
{
    public class AttendanceService
    {
        public string MarkAttendance(MarkAttendenceRequest request)
        {
            try
            {

                var context = new Presence_DBEntities();

                bool AlreadyMarked = context.Attendances.Any(x => x.InstructorID == request.InstructorID && x.CourseID == request.CourseID && x.Late_Minutes_Before != null && x.Late_Minutes_After != null);

                if(AlreadyMarked)
                {
                    return "111";
                }

                var Record = context.Attendances.SingleOrDefault(x => x.InstructorID == request.InstructorID && x.CourseID == request.CourseID && x.Late_Minutes_Before != "");

                if (Record != null)
                {
                    Record.Is_Late_After = request.Is_Late_After;
                    Record.Late_Minutes_After = request.Late_Time_After;

                    context.SaveChanges();

                    return "000";
                }
                else
                {
                    Attendance attendance = new Attendance()
                    {
                        CourseID = request.CourseID,
                        InstructorID = request.InstructorID,
                        Is_Late_Before = request.Is_Late_Before,
                        Late_Minutes_Before = request.Late_Time_Before,
                        InsertionDateTime = DateTime.Now,
                        Shift = request.Shift,
                        ClassID = request.ClassID,
                        Is_Present = true
                    };

                    context.Attendances.Add(attendance);

                    context.SaveChanges();

                    return "000";
                }
            }
            catch
            {
                return "999";
            }
        }

        public List<ShiftScheduleResponse> GetShiftSchedule(string Shift)
        {
            try
            {

                var context = new Presence_DBEntities();

                string date = DateTime.Now.ToString("yyyyMMdd");
                List<ShiftScheduleResponse> response = new List<ShiftScheduleResponse>();

                //var ShiftSchedule = context.ClassSchedules.Where(x =>x.Shift == Shift && x.ClassDate == date).Join(context.Teachers,x=>x.InstructorID == Teacher.).ToList();
                
              var ShiftSchedule = from cs in context.ClassSchedules join tr in context.Teachers on cs.InstructorID equals tr.TeacherID.ToString()
              where (cs.Shift == Shift && cs.ClassDate == date)
              select new {cs.ScheduleID,cs.ClassID,cs.InstructorID,tr.TeacherName,cs.CourseID,cs.Shift,cs.ClassDate,cs.RoomNumber,cs.ClassTime };
                
                if (ShiftSchedule != null && ShiftSchedule.ToList().Count > 0)
                {
                    foreach (var data in ShiftSchedule.ToList())
                    {
                        ShiftScheduleResponse schedule = new ShiftScheduleResponse()
                        {
                            ClassID = data.ClassID,
                            CourseID = data.CourseID,
                            ClassDate = data.ClassDate,
                            InstructorID = data.InstructorID,
                            RoomNumber = data.RoomNumber,
                            ScheduleID = data.ScheduleID,
                            ClassTime = data.ClassTime.ToString(),
                            InstructorName = data.TeacherName,
                            Shift = data.Shift
                        };

                        response.Add(schedule);
                    }                    
                }

                return response;

            }
            catch
            {
                return null;
            }
        }

    }
}