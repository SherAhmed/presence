﻿using Presence.DataAccess.Models;
using Presence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Services
{
    public class UserService
    {
        public Response VerifyLogin(LoginRequest request)
        {
            Response response = new Response();

            var context = new Presence_DBEntities();
            
            bool recordExists = context.Users.Any(x=>x.UserID == request.UserID && x.Password == request.Password && x.Is_Active == "1");

            if(recordExists)
            {
                response.ResponseCode = "000";
                response.ResponseMessage = "Login was successfull.";
            }
            else
            {
                response.ResponseCode = "999";
                response.ResponseMessage = "Invalid User ID or Password.";
            }

            return response;
        }

        public ChangePasswordResponse ChangePassword(ChangePasswordRequest request)
        {
            ChangePasswordResponse response = new ChangePasswordResponse();

            var context = new Presence_DBEntities();

            var userRecord = context.Users.SingleOrDefault(x => x.UserID == request.UserID && x.Password == request.OldPassword && x.Is_Active == "1");

            if (userRecord != null)
            {
                userRecord.Password = request.NewPassword;
                context.SaveChanges();

                response.ResponseCode = "000";
                response.ResponseMessage = "Password Change successfully.";
            }
            else
            {
                response.ResponseCode = "999";
                response.ResponseMessage = "Invalid User ID or Password.";
            }

            return response;
        }

    }
}