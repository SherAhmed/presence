﻿using Presence.Models;
using Presence.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presence.Controllers
{
    public class UserController : ApiController
    {
        [HttpPost]
        [Route("api/Login")]
        public IHttpActionResult Login([FromBody] LoginRequest loginRequest)
        {
            UserService userService = new UserService();
            Response loginResponse = new Response();

            loginResponse = userService.VerifyLogin(loginRequest);

            if (loginResponse.ResponseCode == "000")
            {
                return Content(HttpStatusCode.OK, loginResponse);
            }
            else
            {
                return Content(HttpStatusCode.Unauthorized, loginResponse);
            }
        }

        [HttpPost]
        [Route("api/ChangePassword")]
        public IHttpActionResult ChangePassword([FromBody] ChangePasswordRequest request)
        {
            UserService userService = new UserService();
            ChangePasswordResponse Response = new ChangePasswordResponse();

            Response = userService.ChangePassword(request);

            if (Response.ResponseCode == "000")
            {
                return Content(HttpStatusCode.OK, Response);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, Response);
            }
        }

    }
}
