﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Models
{
    public class Response
    {
        public Response ()
        {
            Values = new List<ShiftScheduleResponse>();
        }

        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public List<ShiftScheduleResponse> Values { get; set; }
    }
}