﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Models
{
    public class LoginRequest
    {
        public string UserID { get; set; }
        public string Password { get; set; }

    }
}