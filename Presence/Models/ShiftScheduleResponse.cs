﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Models
{
    public class ShiftScheduleResponse
    {
        public int ScheduleID { get; set; }
        public string ClassID { get; set; }
        public string InstructorID { get; set; }
        public string CourseID { get; set; }
        public string Shift { get; set; }
        public string ClassDate { get; set; }
        public string RoomNumber { get; set; }
        public string ClassTime { get; set; }
        public string InstructorName { get; set; }
    }
}