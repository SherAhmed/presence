﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Models
{
    public class ChangePasswordResponse
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}