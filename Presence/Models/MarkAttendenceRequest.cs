﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presence.Models
{
    public class MarkAttendenceRequest
    {
        public string InstructorID { get; set; }
        public string CourseID { get; set; }
        public string ClassID { get; set; }
        public bool Is_Late_Before { get; set; }
        public bool Is_Late_After { get; set; }
        public string Late_Time_Before { get; set; }
        public string Late_Time_After { get; set; }
        public string Shift { get; set; }
    }
}