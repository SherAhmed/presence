﻿using Presence.Models;
using Presence.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presence.Controllers
{
    public class AttendanceController : ApiController
    {
        [HttpPost]
        [Route("api/MarkAttendence")]
        public IHttpActionResult MarkAttendence([FromBody] MarkAttendenceRequest request)
        {
            AttendanceService attendanceService = new AttendanceService();
            Response response = new Response();
            
            string ResponseCode = attendanceService.MarkAttendance(request);
            
            if (ResponseCode == "000")
            {
                response.ResponseCode = ResponseCode;
                response.ResponseMessage = "Attendence marked successfully.";

                return Content(HttpStatusCode.OK, response);
            }
            else if (ResponseCode == "111")
            {
                response.ResponseCode = ResponseCode;
                response.ResponseMessage = "Attendence was already marked.";

                return Content(HttpStatusCode.OK, response);
            }
            else
            {
                response.ResponseCode = ResponseCode;
                response.ResponseMessage = "Failed to mark attendence.";
                return Content(HttpStatusCode.Unauthorized, response);
            }
        }

        [HttpGet]
        [Route("api/ShiftSchedule")]
        public IHttpActionResult GetShiftSchedule([FromUri] string Shift)
        {
            AttendanceService attendanceService = new AttendanceService();
            List<ShiftScheduleResponse> shiftSchedule = new List<ShiftScheduleResponse>();
            Response response = new Response();

            shiftSchedule = attendanceService.GetShiftSchedule(Shift);

            if (shiftSchedule != null && shiftSchedule.Count > 0)
            {
                response.ResponseCode = "000";
                response.Values = shiftSchedule;

                return Content(HttpStatusCode.OK, response);
            }
            else
            {
                response.ResponseCode = "999";
                response.ResponseMessage = "Failed to get class schedules.";
                return Content(HttpStatusCode.Unauthorized, response);
            }
        }

    }
}
